import 'dart:ui';

class AppColors {
  static const backgroundColor = Color(0xff210002);
  static const yellow = Color(0xffffaf00);
  static const brown = Color(0xff210002);
  static const orange = Color(0xff973700);
}
