import 'package:flutter/material.dart';

import '../../app_colors.dart';

class BuySection extends StatelessWidget {
  const BuySection({super.key});

  @override
  Widget build(BuildContext context) {
    String title = 'Available now for \$19.99';

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 36.0, vertical: 8.0),
      child: Text(
        title.toUpperCase(),
        style: const TextStyle(
          fontSize: 28,
          color: AppColors.yellow,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}
