import 'package:flutter/material.dart';

import '../../app_colors.dart';

class CopyrightSection extends StatelessWidget {
  const CopyrightSection({super.key});

  @override
  Widget build(BuildContext context) {
    const text = '© 2023  Campo Santo, in cooperation with Panic.'
        '\nFirewatch is a trademark of Campo Santo.'
        '\nNintendo Switch is a trademark of Nintendo.';
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 32.0),
      child: Text(
        text.toUpperCase(),
        style: const TextStyle(
          fontSize: 14,
          color: AppColors.orange,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}
