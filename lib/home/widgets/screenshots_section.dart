import 'package:flutter/material.dart';

import '../../gen/assets.gen.dart';
import 'screenshot_page_view.dart';

class ScreenshotsSection extends StatelessWidget {
  const ScreenshotsSection({super.key});

  @override
  Widget build(BuildContext context) {
    final screenshots = Assets.screenshots.thumbs.values;

    return ListView(
      shrinkWrap: true,
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      physics: const NeverScrollableScrollPhysics(),
      children: [
        for (int i = 0; i < screenshots.length; i++)
          ImageCard(
            imagePath: screenshots[i].path,
            onTap: () {
              Navigator.of(context).push(
                PageRouteBuilder(
                  opaque: false,
                  pageBuilder: (context, _, __) {
                    return ScreenshotPageView(screenshotIndex: i);
                  },
                ),
              );
            },
          )
      ],
    );
  }
}

class ImageCard extends StatelessWidget {
  const ImageCard({
    super.key,
    required this.imagePath,
    required this.onTap,
  });

  final String imagePath;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 32.0),
      child: Stack(
        children: [
          Positioned.fill(
            child: Image.asset(imagePath),
          ),
          Hero(
            tag: imagePath,
            child: AspectRatio(
              aspectRatio: 3 / 2,
              child: Material(
                child: Ink.image(
                  image: AssetImage(imagePath),
                  fit: BoxFit.cover,
                  child: InkWell(
                    onTap: onTap,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
