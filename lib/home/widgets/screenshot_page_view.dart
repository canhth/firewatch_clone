import 'package:flutter/material.dart';

import '../../gen/assets.gen.dart';

class ScreenshotPageView extends StatefulWidget {
  const ScreenshotPageView({
    super.key,
    required this.screenshotIndex,
  });

  final int screenshotIndex;

  @override
  State<ScreenshotPageView> createState() => _ScreenshotPageViewState();
}

class _ScreenshotPageViewState extends State<ScreenshotPageView> {
  late int screenshotIndex;

  late final PageController pageController;

  final screenshots = Assets.screenshots.thumbs.values;

  @override
  void initState() {
    super.initState();
    screenshotIndex = screenshots.length * 999 + widget.screenshotIndex;
    pageController = PageController(initialPage: screenshotIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black54,
      body: Stack(
        children: [
          Positioned.fill(
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AspectRatio(
                  aspectRatio: 3 / 2,
                  child: SizedBox(
                    height: 400,
                    child: PageView.builder(
                      controller: pageController,
                      itemBuilder: (context, index) {
                        final image = screenshots[index % screenshots.length];

                        return Stack(
                          alignment: Alignment.center,
                          fit: StackFit.expand,
                          children: [
                            Positioned(
                              child: GestureDetector(
                                onTap: () {
                                  pageController.nextPage(
                                    duration: const Duration(milliseconds: 300),
                                    curve: Curves.easeIn,
                                  );
                                },
                                child: Hero(
                                  tag: image.path,
                                  child: Image.asset(
                                    image.path,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: _IconButton(
                                icon: Icons.arrow_left,
                                onTap: () {
                                  pageController.previousPage(
                                    duration: const Duration(milliseconds: 300),
                                    curve: Curves.easeIn,
                                  );
                                },
                              ),
                            ),
                            Align(
                              alignment: Alignment.centerRight,
                              child: _IconButton(
                                icon: Icons.arrow_right,
                                onTap: () {
                                  pageController.nextPage(
                                    duration: const Duration(milliseconds: 300),
                                    curve: Curves.easeIn,
                                  );
                                },
                              ),
                            )
                          ],
                        );
                      },
                      onPageChanged: (value) {
                        if (value != screenshotIndex) {
                          setState(() {
                            screenshotIndex = value;
                          });
                        }
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    '${screenshotIndex % screenshots.length + 1} of ${screenshots.length}',
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _IconButton extends StatelessWidget {
  const _IconButton({
    super.key,
    required this.icon,
    required this.onTap,
  });

  final IconData icon;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Icon(
            icon,
            size: 60,
            color: Colors.grey,
          ),
          Icon(
            icon,
            size: 50,
            color: Colors.white,
          ),
        ],
      ),
    );
  }
}
