import 'package:flutter/material.dart';

import '../../gen/assets.gen.dart';

class KeyArtParallax extends StatefulWidget {
  const KeyArtParallax({super.key});

  @override
  State<KeyArtParallax> createState() => _ArtParallaxState();
}

class _ArtParallaxState extends State<KeyArtParallax> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 400,
      child: Stack(
        fit: StackFit.expand,
        children: [
          for (var i = 0; i < Assets.images.parallax.values.length; i++)
            _KeyArtLayerParallax(
              image: Assets.images.parallax.values.elementAt(i).path,
              imageIndex: i,
            )
        ],
      ),
    );
  }
}

class _KeyArtLayerParallax extends StatelessWidget {
  _KeyArtLayerParallax({
    super.key,
    required this.image,
    required this.imageIndex,
  });

  final String image;
  final int imageIndex;

  final GlobalKey _imageKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Flow(
      delegate: ParallaxFlowDelegate(
        scrollableState: Scrollable.of(context),
        layerItemContext: context,
        imageKey: _imageKey,
        imageIndex: imageIndex,
      ),
      children: [
        Image.asset(
          image,
          key: _imageKey,
          fit: BoxFit.fitHeight,
        )
      ],
    );
  }
}

class ParallaxFlowDelegate extends FlowDelegate {
  final ScrollableState scrollableState;
  final BuildContext layerItemContext;
  final GlobalKey imageKey;
  final int imageIndex;

  ParallaxFlowDelegate({
    required this.scrollableState,
    required this.layerItemContext,
    required this.imageKey,
    required this.imageIndex,
  }) : super(repaint: scrollableState.position);

  @override
  void paintChildren(FlowPaintingContext context) {
    // level = 1.0 => image will stay in position when scrolling
    // The higher the level, the less the image will translate
    final List<double> levels = [1.0, 1.5, 1.2, 2.5, 3, 3.5, 10, 15, 20];

    double dy = scrollableState.position.pixels / levels[imageIndex];

    final offset = Offset(0.0, dy);

    context.paintChild(
      0,
      transform: Transform.translate(
        offset: offset,
      ).transform,
    );
  }

  @override
  bool shouldRepaint(ParallaxFlowDelegate oldDelegate) {
    return scrollableState != oldDelegate.scrollableState ||
        layerItemContext != oldDelegate.layerItemContext ||
        imageKey != oldDelegate.imageKey;
  }
}
