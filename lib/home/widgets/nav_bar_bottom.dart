import 'package:flutter/material.dart';

import '../../app_colors.dart';

class NavBarBottom extends StatelessWidget {
  const NavBarBottom({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _Button(
          title: 'Campo Santo',
          onTap: () {},
        ),
        _Button(
          title: 'Quarterly Review',
          onTap: () {},
        ),
        _Button(
          title: 'development blog',
          onTap: () {},
        ),
        _Button(
          title: 'firewatch',
          onTap: () {},
          backgroundColor: AppColors.orange,
        ),
      ],
    );
  }
}

class _Button extends StatelessWidget {
  const _Button({
    super.key,
    required this.title,
    required this.onTap,
    this.backgroundColor = AppColors.backgroundColor,
    this.foregroundColor = AppColors.yellow,
  });

  final String title;
  final VoidCallback onTap;
  final Color backgroundColor;
  final Color foregroundColor;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Material(
        color: backgroundColor,
        child: InkWell(
          onTap: onTap,
          hoverColor: Colors.white,
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: Text(
              title.toUpperCase(),
              style: TextStyle(
                color: foregroundColor,
                fontSize: 20,
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
