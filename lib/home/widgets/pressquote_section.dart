import 'package:flutter/material.dart';

import '../../app_colors.dart';

class PressquoteSection extends StatelessWidget {
  const PressquoteSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: '"As visually striking\nas its unique premise."'.toUpperCase(),
          style: const TextStyle(
            fontSize: 36,
            fontWeight: FontWeight.w500,
            color: AppColors.yellow,
          ),
          children: [
            TextSpan(
              text: '\n\nEntertainment Weekly'.toUpperCase(),
              style: const TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w500,
                color: AppColors.orange,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
