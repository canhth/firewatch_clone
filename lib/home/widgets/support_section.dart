import 'package:flutter/material.dart';

import '../../app_colors.dart';
import 'ticket_button.dart';

class SupportSection extends StatelessWidget {
  const SupportSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24),
      child: Column(
        children: [
          TicketButton(
            title: 'Tech Support'.toUpperCase(),
            onPressed: () {},
            backgroundColor: AppColors.orange,
          ),
          const SizedBox(height: 24),
          TicketButton(
            title: 'Streaming & Let\'s Plays'.toUpperCase(),
            onPressed: () {},
            backgroundColor: AppColors.orange,
          ),
        ],
      ),
    );
  }
}
