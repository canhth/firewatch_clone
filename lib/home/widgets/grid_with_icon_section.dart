import 'package:flutter/material.dart';

import '../../gen/assets.gen.dart';
import 'ticket_button.dart';

class GridWithIconSection extends StatelessWidget {
  const GridWithIconSection({super.key});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 36.0, vertical: 16.0),
      child: Wrap(
        alignment: WrapAlignment.center,
        runSpacing: 24,
        spacing: 24,
        children: [
          TicketButton(
            title: 'window mac linux',
            iconPath: Assets.images.buyiconPc.path,
            onPressed: () {},
          ),
          TicketButton(
            title: 'playstation 4',
            iconPath: Assets.images.buyiconPs4.path,
            onPressed: () {},
          ),
          TicketButton(
            title: 'nintendo switch',
            iconPath: Assets.images.buyiconSwitch.path,
            onPressed: () {},
          ),
          TicketButton(
            title: 'xbox one',
            iconPath: Assets.images.buyiconXbone.path,
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
