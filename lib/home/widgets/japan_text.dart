import 'package:flutter/material.dart';

import '../../app_colors.dart';

class JapanText extends StatelessWidget {
  const JapanText({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(16.0),
      child: Text(
        '日本語に関する情報',
        style: TextStyle(
          fontSize: 36,
          fontWeight: FontWeight.bold,
          letterSpacing: 5,
          color: AppColors.yellow,
        ),
      ),
    );
  }
}
