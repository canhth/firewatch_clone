import 'package:flutter/material.dart';

import '../../app_colors.dart';

class DescriptionSection extends StatelessWidget {
  const DescriptionSection({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(
        top: 60,
        left: 16,
        right: 16,
        bottom: 30,
      ),
      child: Column(
        children: [
          Text(
            'Firewatch is a mystery set in the Wyoming wilderness, where your only emotional lifeline is the person on the other end of a handheld radio.',
            style: TextStyle(
              fontSize: 28,
              color: AppColors.yellow,
              fontWeight: FontWeight.w500,
            ),
          ),
          Text(
            '\n\nThe year is 1989.'
            '\n\nYou are a man named Henry who has retreated from your messy life to work as a fire lookout in the Wyoming wilderness. Perched atop a mountain, it\'s your job to find smoke and keep the wilderness safe.'
            '\n\nAn especially hot, dry summer has everyone on edge. Your supervisor, a woman named Delilah, is available to you at all times over a small, handheld radio—and is your only contact with the world you\'ve left behind.'
            '\n\nBut when something strange draws you out of your lookout tower and into the world below, you\'ll explore a wild and unknown environment, facing questions and making interpersonal choices that can build or destroy the only meaningful relationship you have.',
            style: TextStyle(
              fontSize: 18,
              color: AppColors.yellow,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }
}
