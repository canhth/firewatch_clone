import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoYoutubeEmbed extends StatefulWidget {
  const VideoYoutubeEmbed({
    super.key,
    required this.videoUrl,
    this.padding = const EdgeInsets.all(16.0),
  });

  final String videoUrl;
  final EdgeInsets padding;

  @override
  State<VideoYoutubeEmbed> createState() => _VideoYoutubeEmbedState();
}

class _VideoYoutubeEmbedState extends State<VideoYoutubeEmbed> {
  late final String videoId;
  late YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();

    videoId = YoutubePlayer.convertUrlToId(widget.videoUrl)!;

    _controller = YoutubePlayerController(
      initialVideoId: videoId,
      flags: const YoutubePlayerFlags(
        autoPlay: false,
        mute: false,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding,
      child: YoutubePlayer(
        controller: _controller,
        showVideoProgressIndicator: true,
        progressIndicatorColor: Colors.amber,
        progressColors: const ProgressBarColors(
          playedColor: Colors.amber,
          handleColor: Colors.amberAccent,
        ),
        onReady: () {
          // _controller.addListener(listener);
        },
      ),
    );
  }
}
