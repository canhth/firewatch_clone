import 'package:flutter/material.dart';

import '../../app_colors.dart';
import '../../gen/assets.gen.dart';

class CompanylinksSection extends StatelessWidget {
  const CompanylinksSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 50.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            color: AppColors.orange,
            margin: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Image.asset(
              Assets.images.logoCamposantoTransparent.path,
              width: 125,
            ),
          ),
          Container(
            color: AppColors.orange,
            margin: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Image.asset(
              Assets.images.logoPanicTransparent.path,
              width: 60,
            ),
          ),
        ],
      ),
    );
  }
}
