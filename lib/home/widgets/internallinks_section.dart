import 'package:flutter/material.dart';

import '../../app_colors.dart';
import 'ticket_button.dart';

class InternallinksSection extends StatelessWidget {
  const InternallinksSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TicketButton(
          title: 'Screens & Trailers',
          onPressed: () {},
          padding: const EdgeInsets.symmetric(
            vertical: 16.0,
            horizontal: 36,
          ),
          backgroundColor: AppColors.orange,
        ),
        TicketButton(
          title: 'Firewatch FAQ',
          onPressed: () {},
          padding: const EdgeInsets.symmetric(
            vertical: 16.0,
            horizontal: 36,
          ),
          backgroundColor: AppColors.orange,
        ),
      ],
    );
  }
}
