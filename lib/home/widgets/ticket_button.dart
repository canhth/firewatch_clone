import 'package:flutter/material.dart';

import '../../app_colors.dart';
import '../../gen/assets.gen.dart';

class TicketButton extends StatelessWidget {
  const TicketButton({
    super.key,
    required this.title,
    this.iconPath,
    required this.onPressed,
    this.padding = EdgeInsets.zero,
    this.backgroundColor,
  });

  final String title;
  final String? iconPath;
  final VoidCallback onPressed;
  final EdgeInsets padding;
  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Material(
        color: backgroundColor ?? AppColors.yellow,
        child: InkWell(
          onTap: onPressed,
          child: Stack(
            children: [
              Positioned(
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16.0,
                    vertical: 8.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (iconPath != null)
                        Image.asset(iconPath!, width: 34, height: 20),
                      if (iconPath != null) const SizedBox(width: 16),
                      Text(
                        title.toUpperCase(),
                        style: const TextStyle(
                          color: AppColors.brown,
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                left: 0,
                top: 0,
                bottom: 0,
                child: Image.asset(Assets.images.bannerTransparentLeft.path),
              ),
              Positioned(
                right: 0,
                top: 0,
                bottom: 0,
                child: Image.asset(Assets.images.bannerTransparentRight.path),
              )
            ],
          ),
        ),
      ),
    );
  }
}
