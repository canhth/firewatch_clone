import 'package:flutter/material.dart';

import '../app_colors.dart';
import 'widgets/buy_section.dart';
import 'widgets/companylinks_section.dart';
import 'widgets/copyright_section.dart';
import 'widgets/description_section.dart';
import 'widgets/grid_with_icon_section.dart';
import 'widgets/internallinks_section.dart';
import 'widgets/japan_text.dart';
import 'widgets/key_art_parallax.dart';
import 'widgets/nav_bar_bottom.dart';
import 'widgets/pressquote_section.dart';
import 'widgets/screenshots_section.dart';
import 'widgets/support_section.dart';
import 'widgets/video_youtube_embed.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            KeyArtParallax(),
            BuySection(),
            GridWithIconSection(),
            VideoYoutubeEmbed(
              videoUrl: 'https://www.youtube.com/watch?v=cXWlgP5hZzc',
            ),
            DescriptionSection(),
            InternallinksSection(),
            ScreenshotsSection(),
            PressquoteSection(),
            SupportSection(),
            JapanText(),
            CopyrightSection(),
            CompanylinksSection(),
            NavBarBottom(),
          ],
        ),
      ),
    );
  }
}
