/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/banner_transparent_left.png
  AssetGenImage get bannerTransparentLeft =>
      const AssetGenImage('assets/images/banner_transparent_left.png');

  /// File path: assets/images/banner_transparent_right.png
  AssetGenImage get bannerTransparentRight =>
      const AssetGenImage('assets/images/banner_transparent_right.png');

  /// File path: assets/images/buyicon_pc.png
  AssetGenImage get buyiconPc =>
      const AssetGenImage('assets/images/buyicon_pc.png');

  /// File path: assets/images/buyicon_ps4.png
  AssetGenImage get buyiconPs4 =>
      const AssetGenImage('assets/images/buyicon_ps4.png');

  /// File path: assets/images/buyicon_switch.png
  AssetGenImage get buyiconSwitch =>
      const AssetGenImage('assets/images/buyicon_switch.png');

  /// File path: assets/images/buyicon_xbone.png
  AssetGenImage get buyiconXbone =>
      const AssetGenImage('assets/images/buyicon_xbone.png');

  /// File path: assets/images/firewatch_shield_small.png
  AssetGenImage get firewatchShieldSmall =>
      const AssetGenImage('assets/images/firewatch_shield_small.png');

  /// File path: assets/images/keyart-mobile.jpg
  AssetGenImage get keyartMobile =>
      const AssetGenImage('assets/images/keyart-mobile.jpg');

  /// File path: assets/images/logo_camposanto_transparent.png
  AssetGenImage get logoCamposantoTransparent =>
      const AssetGenImage('assets/images/logo_camposanto_transparent.png');

  /// File path: assets/images/logo_panic_transparent.png
  AssetGenImage get logoPanicTransparent =>
      const AssetGenImage('assets/images/logo_panic_transparent.png');

  $AssetsImagesParallaxGen get parallax => const $AssetsImagesParallaxGen();

  /// List of all assets
  List<AssetGenImage> get values => [
        bannerTransparentLeft,
        bannerTransparentRight,
        buyiconPc,
        buyiconPs4,
        buyiconSwitch,
        buyiconXbone,
        firewatchShieldSmall,
        keyartMobile,
        logoCamposantoTransparent,
        logoPanicTransparent
      ];
}

class $AssetsScreenshotsGen {
  const $AssetsScreenshotsGen();

  $AssetsScreenshotsThumbsGen get thumbs => const $AssetsScreenshotsThumbsGen();
}

class $AssetsImagesParallaxGen {
  const $AssetsImagesParallaxGen();

  /// File path: assets/images/parallax/parallax0.png
  AssetGenImage get parallax0 =>
      const AssetGenImage('assets/images/parallax/parallax0.png');

  /// File path: assets/images/parallax/parallax1.png
  AssetGenImage get parallax1 =>
      const AssetGenImage('assets/images/parallax/parallax1.png');

  /// File path: assets/images/parallax/parallax2.png
  AssetGenImage get parallax2 =>
      const AssetGenImage('assets/images/parallax/parallax2.png');

  /// File path: assets/images/parallax/parallax3.png
  AssetGenImage get parallax3 =>
      const AssetGenImage('assets/images/parallax/parallax3.png');

  /// File path: assets/images/parallax/parallax4.png
  AssetGenImage get parallax4 =>
      const AssetGenImage('assets/images/parallax/parallax4.png');

  /// File path: assets/images/parallax/parallax5.png
  AssetGenImage get parallax5 =>
      const AssetGenImage('assets/images/parallax/parallax5.png');

  /// File path: assets/images/parallax/parallax6.png
  AssetGenImage get parallax6 =>
      const AssetGenImage('assets/images/parallax/parallax6.png');

  /// File path: assets/images/parallax/parallax7.png
  AssetGenImage get parallax7 =>
      const AssetGenImage('assets/images/parallax/parallax7.png');

  /// File path: assets/images/parallax/parallax8.png
  AssetGenImage get parallax8 =>
      const AssetGenImage('assets/images/parallax/parallax8.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        parallax0,
        parallax1,
        parallax2,
        parallax3,
        parallax4,
        parallax5,
        parallax6,
        parallax7,
        parallax8
      ];
}

class $AssetsScreenshotsThumbsGen {
  const $AssetsScreenshotsThumbsGen();

  /// File path: assets/screenshots/thumbs/firewatch_01.jpg
  AssetGenImage get firewatch01 =>
      const AssetGenImage('assets/screenshots/thumbs/firewatch_01.jpg');

  /// File path: assets/screenshots/thumbs/firewatch_02.jpg
  AssetGenImage get firewatch02 =>
      const AssetGenImage('assets/screenshots/thumbs/firewatch_02.jpg');

  /// File path: assets/screenshots/thumbs/firewatch_03.jpg
  AssetGenImage get firewatch03 =>
      const AssetGenImage('assets/screenshots/thumbs/firewatch_03.jpg');

  /// File path: assets/screenshots/thumbs/firewatch_04.jpg
  AssetGenImage get firewatch04 =>
      const AssetGenImage('assets/screenshots/thumbs/firewatch_04.jpg');

  /// List of all assets
  List<AssetGenImage> get values =>
      [firewatch01, firewatch02, firewatch03, firewatch04];
}

class Assets {
  Assets._();

  static const $AssetsImagesGen images = $AssetsImagesGen();
  static const $AssetsScreenshotsGen screenshots = $AssetsScreenshotsGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
